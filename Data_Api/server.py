from flask import Flask, render_template, make_response, request, jsonify, send_file
from storage import Storage
import json
import os

app = Flask(__name__)
users = Storage['users']
plays = Storage['plays']

@app.route('/', methods=['POST'])
def main():
	return make_response('Hi! If you are receiving this message, congratulations! You are now accessing SAC2018 Data API.')

@app.route('/login', methods=['POST'])
def login():
	user = request.form.get('User')
	password = request.form.get('Password')

	if user in users.keys():
		if users[user]['password'] == password:
			return make_response('sucess_logged_in', 200)
		else:
			return make_response('incorrect_password', 401)
	else:
		return make_response('user_does_not_exist', 401)

@app.route('/finish', methods=['POST'])
def finish():
	user = request.form.get('User')
	coins = request.form.get('Coins')
	deaths = request.form.get('Deaths')
	checkpoints = request.form.get('Checkpoints')

	entry = {'user': user, 'coins': coins, 'deaths': deaths, 'checkpoints': checkpoints}

	Storage['plays'].write(json.dumps(entry) + '\n')

	return make_response('finished_stage', 200)

if __name__ == "__main__":
	app.run(host='0.0.0.0', port=5000, debug=True)