import json
import os

dirname = os.path.dirname(__file__)

__users_file = open(os.path.join(dirname, 'users.json'))
__users_read = __users_file.read()
__users_data = json.loads(__users_read)

__plays_file = open(os.path.join(dirname, 'plays.json'), 'a')

Storage = dict(
	users=__users_data,
	plays=__plays_file
)
